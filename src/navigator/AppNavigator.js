import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import Login from '../screens/Login';
import ChangeServer from '../screens/ChangeServer';
import {COLOR} from '../helpers/constants';

const Stack = createStackNavigator();

function AppNavigator() {
  return (
    <Stack.Navigator
      screenOptions={{
        headerTitleStyle: {
          fontSize: 22,
        },
        headerStyle: {
          backgroundColor: COLOR.PRIMARY,
        },
        headerTintColor: COLOR.PRIMARY_TEXT,
      }}>
      <Stack.Screen
        name="Login"
        component={Login}
        options={{
          headerTitle: 'Smart Department',
        }}
      />
      <Stack.Screen
        name="ChangeServer"
        component={ChangeServer}
        options={{
          headerTitle: 'Change Server',
        }}
      />
    </Stack.Navigator>
  );
}

export default AppNavigator;
