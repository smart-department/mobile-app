import React from 'react';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import {createStackNavigator} from '@react-navigation/stack';
import System from '../screens/System';
import SystemAdd from '../screens/SystemAdd';
import SystemIndividual from '../screens/SystemIndividual';
import User from '../screens/User';
import UserAdd from '../screens/UserAdd';
import UserAll from '../screens/UserAll';
import {COLOR} from '../helpers/constants';

const Tab = createMaterialTopTabNavigator();
const Stack = createStackNavigator();

export function FrontScreen() {
  return (
    <Tab.Navigator
      tabBarOptions={{
        activeTintColor: COLOR.PRIMARY_TEXT,
        inactiveTintColor: `${COLOR.PRIMARY_TEXT}77`,
        labelStyle: {
          fontSize: 14,
          fontWeight: 'bold',
        },
        indicatorStyle: {
          height: 3,
          backgroundColor: COLOR.PRIMARY_TEXT,
        },
        style: {
          backgroundColor: COLOR.PRIMARY,
          elevation: 2,
        },
      }}>
      <Tab.Screen
        name="System"
        component={System}
        options={{
          title: 'System',
        }}
      />
      <Tab.Screen
        name="Profile"
        component={User}
        options={{headerShown: false}}
      />
    </Tab.Navigator>
  );
}

function AdminNavigator() {
  return (
    <Stack.Navigator
      screenOptions={{
        headerTitleStyle: {
          fontSize: 22,
        },
        headerStyle: {
          backgroundColor: COLOR.PRIMARY,
        },
        headerTintColor: COLOR.PRIMARY_TEXT,
      }}>
      <Stack.Screen
        name="FrontScreen"
        component={FrontScreen}
        options={{
          headerTitle: 'Smart Department',
          headerStyle: {
            backgroundColor: COLOR.PRIMARY,
            elevation: 0,
          },
        }}
      />
      <Stack.Screen
        name="SystemAdd"
        component={SystemAdd}
        options={{
          headerTitle: 'Add System',
        }}
      />
      <Stack.Screen
        name="SystemIndividual"
        component={SystemIndividual}
        options={({route}) => ({
          title: route.params.name,
        })}
      />
      <Stack.Screen
        name="UserAdd"
        component={UserAdd}
        options={{
          headerTitle: 'Add User',
        }}
      />
      <Stack.Screen
        name="UserAll"
        component={UserAll}
        options={{
          headerTitle: 'Users List',
        }}
      />
    </Stack.Navigator>
  );
}

export default AdminNavigator;
