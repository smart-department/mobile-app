import React, {useState, useEffect} from 'react';
import {
  ScrollView,
  ActivityIndicator,
  StyleSheet,
  Alert,
  ToastAndroid,
} from 'react-native';
import fire from '../../requests/fire';
import {
  VeryBasicInfo,
  BasicInfo,
  Overview,
  Memory,
  Processor,
  DiskUsage,
  Actions,
} from './SystemMore';

function SystemMoreInfo({navigation, id}) {
  const [system, setSystem] = useState();
  const [showRetry, setShowRetry] = useState(false);
  const [refreshing, setResfreshing] = useState(false);
  const [deleting, setDeleting] = useState(false);
  const [shuttingDown, setShuttingDown] = useState(false);

  useEffect(() => {
    fetchSystemInfo();
    const interval = setInterval(() => fetchSystemInfo(), 3000);
    return () => {
      clearInterval(interval);
    };
  }, []);

  const fetchSystemInfo = async () => {
    if (refreshing) {
      return;
    }
    try {
      setResfreshing(true);
      const currentSystem = await fire('getSystem', {}, {id});
      setSystem(currentSystem);

      if (currentSystem.info.message) {
        setShowRetry(true);
      } else {
        setShowRetry(false);
      }
    } catch (exception) {
      setShowRetry(true);
    } finally {
      setResfreshing(false);
    }
  };

  const shutdownAction = async () => {
    try {
      setShuttingDown(true);
      await fire('shutdownSystem', {}, {id});
      setTimeout(() => {
        navigation.navigate('System', {refresh: true});
        setShuttingDown(false);
      }, 1000);
    } catch (exception) {
      console.log('Shutdown failed');
      setShuttingDown(false);
    } finally {
    }
  };

  const deleteAction = async () => {
    Alert.alert(
      'Confirm Deletion',
      `Are you sure you want to delete system '${system.name}'?`,
      [
        {
          text: 'Cancel',
          onPress: () => {},
          style: 'cancel',
        },
        {
          text: 'OK',
          onPress: async () => {
            try {
              setDeleting(true);
              await fire('deleteSystem', {}, {id});
              ToastAndroid.show(`Deleted '${system.name}'`, ToastAndroid.SHORT);
              navigation.navigate('System', {refresh: true});
            } catch (exception) {
              ToastAndroid.show(
                `Couldn't delete '${system.name}'`,
                ToastAndroid.SHORT,
              );
            } finally {
              setDeleting(false);
            }
          },
        },
      ],
      {cancelable: true},
    );
  };

  if (!system) {
    return (
      <ActivityIndicator
        color="#000000"
        size={52}
        style={styles.activityIndicator}
      />
    );
  }

  return (
    <ScrollView>
      <VeryBasicInfo
        system={{
          last_update: system.last_update,
          visibility: system.visibility,
          created: system.created,
          endpoint: system.endpoint,
        }}
        systemDown={system.info.message}
      />
      <Actions
        showRetry={showRetry}
        refreshing={refreshing}
        refresh={fetchSystemInfo}
        deleting={deleting}
        deleteAction={deleteAction}
        shutdownAction={shutdownAction}
        shuttingDown={shuttingDown}
      />
      {!system.info.message && (
        <>
          <BasicInfo data={system.info.meta} />
          <Overview
            data={{
              load_average: system.info.load_average.join(', '),
              uptime: system.info.uptime,
            }}
            refreshing={refreshing}
          />
          <Memory data={system.info.memory} refreshing={refreshing} />
          <Processor data={system.info.cpu} refreshing={refreshing} />
          <DiskUsage data={system.info.disk} refreshing={refreshing} />
        </>
      )}
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  activityIndicator: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default SystemMoreInfo;
