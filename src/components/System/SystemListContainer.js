import React, {useState, useEffect, useContext, useCallback} from 'react';
import {FlatList} from 'react-native';
import fire from '../../requests/fire';
import SystemInfo from './SystemInfo';
import UserContext from '../../context/UserContext';
import {FloatingBottomButton} from '../Common/Buttons';

function SystemsListContainer({navigation, refresh}) {
  const [systems, setSystems] = useState(false);
  const [refreshing, setRefreshing] = useState(false);
  const user = useContext(UserContext)[0];
  useEffect(() => {
    fetchSystems();
  }, [fetchSystems, systems, refresh]);

  const fetchSystems = useCallback(async () => {
    if (refreshing) {
      return;
    }
    setRefreshing(true);
    try {
      const currentSystems = await fire('allSystems');
      setSystems(currentSystems);
    } catch (exception) {
    } finally {
      setRefreshing(false);
    }
  }, [refreshing]);

  return (
    <>
      <FlatList
        data={systems}
        renderItem={({item}) => (
          <SystemInfo
            system={item}
            navigation={user.role === 'admin' ? navigation : false}
          />
        )}
        keyExtractor={item => item.id}
        refreshing={refreshing}
        onRefresh={fetchSystems}
      />
      {user.role === 'admin' && (
        <FloatingBottomButton
          type="add"
          onPress={() => navigation.navigate('SystemAdd')}
        />
      )}
    </>
  );
}

export default SystemsListContainer;
