import React, {useState} from 'react';
import {ToastAndroid} from 'react-native';
import {Button} from '../Common/Buttons';
import {InputBox, Option} from '../Common/Inputs';
import fire from '../../requests/fire';
import FormContainer from '../Common/FormContainer';
import {Error} from '../Common/Texts';

function UserAddForm({goBack}) {
  const [loading, setLoading] = useState(false);
  const [form, setForm] = useState({
    name: '',
    system_key: '',
    endpoint: '',
    visibility: 'all',
    error: '',
  });

  function handleChange(e) {
    const {name, value} = e.target;
    setForm({...form, [name]: value});
  }

  async function handleSubmit() {
    setLoading(true);
    try {
      const currentSystem = {...form};
      delete currentSystem.error;

      await fire('createSystem', currentSystem);
      ToastAndroid.show(
        `Created system '${currentSystem.name}'`,
        ToastAndroid.SHORT,
      );
      goBack();
    } catch (exception) {
      ToastAndroid.show("Couldn't create system", ToastAndroid.SHORT);
      setForm({...form, error: exception});
      setLoading(false);
    }
  }

  return (
    <FormContainer error={form.error}>
      <InputBox
        value={form.name}
        label="Name"
        placeholder="Name for the system"
        name="name"
        onChange={handleChange}
      />
      <InputBox
        value={form.system_key}
        label="System Key"
        placeholder="**********"
        name="system_key"
        secureTextEntry={true}
        onChange={handleChange}
      />
      <InputBox
        value={form.endpoint}
        label="Endpoint"
        placeholder="Endpoint for information"
        name="endpoint"
        onChange={handleChange}
      />
      <Option
        name="visibility"
        onChange={handleChange}
        label="Visibility of the system"
        value={form.visibility}
        options={[
          {label: 'All', value: 'all'},
          {label: 'Restricted', value: 'restricted'},
        ]}
      />

      <Error>{form.error}</Error>
      <Button onPress={handleSubmit} loading={loading} title="Add System" />
    </FormContainer>
  );
}

export default UserAddForm;
