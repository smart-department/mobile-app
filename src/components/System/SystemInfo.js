import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {Button, Labels} from '../Common/Buttons';
import {dateString, memoryString} from '../../helpers';
import {COLOR} from '../../helpers/constants';

function SystemInfo({system, navigation}) {
  const {id, name, info, last_update} = system;

  function showDetails() {
    return (
      <View style={styles.container}>
        <View style={styles.heading}>
          <Text
            ellipsizeMode={'tail'}
            numberOfLines={1}
            style={styles.headingText}>
            {name}
          </Text>
          <Labels
            titles={info.message ? ['down'] : ['active']}
            danger={info.message}
          />
        </View>
        <View style={styles.moreInfo}>
          {info.message ? (
            <View>
              <Text style={styles.infoText}>{info.message}</Text>
              {last_update ? (
                <Text style={styles.infoText}>
                  Last seen on {dateString(last_update)}
                </Text>
              ) : (
                <Text style={styles.infoText}>No last seen</Text>
              )}
            </View>
          ) : (
            <View>
              <Text style={styles.infoText}>
                Load: {info.load_average.join(', ')}
              </Text>
              <Text style={styles.infoText}>
                Memory: {memoryString(info.memory.used)} /{' '}
                {memoryString(info.memory.total)}
              </Text>
            </View>
          )}
        </View>
      </View>
    );
  }

  return navigation ? (
    <Button
      style={styles.button}
      onPress={() => navigation.navigate('SystemIndividual', {id, name})}>
      {showDetails()}
    </Button>
  ) : (
    showDetails()
  );
}

const styles = StyleSheet.create({
  button: {
    backgroundColor: 'transparent',
    elevation: 0,
    borderRadius: 0,
    justifyContent: 'flex-start',
    paddingTop: 0,
    paddingBottom: 0,
    paddingHorizontal: 0,
  },
  container: {
    flex: 1,
    maxWidth: '100%',
    paddingVertical: 25,
    paddingHorizontal: 15,
    borderBottomWidth: 0.75,
    borderBottomColor: '#E2E8F0',
  },
  heading: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    paddingBottom: 5,
  },
  headingText: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  infoText: {
    color: `${COLOR.TEXT}AA`,
  },
});

export default SystemInfo;
