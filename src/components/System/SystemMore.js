import React from 'react';
import {View, Text, StyleSheet, Linking, ActivityIndicator} from 'react-native';
import {Labels, Button, DeleteButton} from '../Common/Buttons';
import {dateString, memoryString} from '../../helpers';
import {Link} from '../Common/Buttons';
import {COLOR} from '../../helpers/constants';

export function VeryBasicInfo({system, systemDown}) {
  if (!system) {
    return null;
  }

  return (
    <View style={{...styles.container, ...styles.veryBasicInfoContainer}}>
      {systemDown ? (
        <View>
          <Text>{systemDown}</Text>
          <Text>
            {system.last_update === null
              ? 'No last seen'
              : `Last seen on ${dateString(system.last_update)}`}
          </Text>
        </View>
      ) : (
        <View style={styles.labels}>
          <Link
            style={styles.info}
            title="Open Client Info"
            onPress={() => Linking.openURL(`${system.endpoint}/info.php`)}
          />
          <Info text={dateString(system.created)} />
        </View>
      )}
      <View />
      <Labels
        style={styles.labels}
        titles={[systemDown ? 'down' : 'active', system.visibility]}
        danger={systemDown}
      />
    </View>
  );
}

export function BasicInfo({data}) {
  return (
    <InformationSchema title="Basic Information">
      <Info text={`OS: ${data.os} ${data.machine_type}`} />
      <Info text={`Release: ${data.variant} ${data.release}`} />
      <Info text={`Hostname: ${data.host}`} />
      <Info text={`IP: ${data.ip.replace('\n', ', ') || 'Nil'}`} />
    </InformationSchema>
  );
}

export function Overview({data}) {
  return (
    <InformationSchema title="Overview">
      <Info text={`Load: ${data.load_average}`} />
      <Info text={`Uptime: ${data.uptime}`} />
    </InformationSchema>
  );
}

export function Memory({data}) {
  return (
    <InformationSchema title="Memory">
      <Info
        text={`Memory: ${memoryString(data.ram.used)} / ${memoryString(
          data.ram.total,
        )}`}
      />
      <Info
        text={`Swap: ${memoryString(data.swap.used)} / ${memoryString(
          data.swap.total,
        )}`}
      />
    </InformationSchema>
  );
}

export function Processor({data}) {
  return (
    <InformationSchema title="Processor">
      {data.map(cpu => (
        <View key={cpu.model_id}>
          <Info text={`Vendor: ${cpu.vendor}`} />
          <Info text={`Model: ${cpu.model}`} />
          <Info text={`Cores: ${cpu.cores}`} />
          <Info text={`Threads: ${cpu.threads}`} />
          <Info text={`Clock Rate: ${cpu.clock_rate_average.toFixed(3)}MHz`} />
          {cpu.clock_rate_max && cpu.clock_rate_min && (
            <Info
              text={`Clock Rate Range: ${cpu.clock_rate_min.toFixed(
                3,
              )} - ${cpu.clock_rate_max.toFixed(3)} MHz`}
            />
          )}
        </View>
      ))}
    </InformationSchema>
  );
}

export function DiskUsage({data}) {
  return (
    <InformationSchema title="Disk Usage">
      <View>
        <View style={styles.row}>
          <Info
            style={styles.rowElemOne}
            textStyle={styles.tableHeading}
            text="Mounted"
          />
          <Info
            style={styles.rowElemOne}
            textStyle={styles.tableHeading}
            text="File System"
          />
          <Info
            style={styles.rowElemTwo}
            textStyle={styles.tableHeading}
            text="Usage"
          />
        </View>
        {data.map(disk => (
          <View style={styles.row} key={disk.mounted}>
            <Info style={styles.rowElemOne} text={disk.mounted} />
            <Info style={styles.rowElemOne} text={disk.filesystem} />
            <Info
              style={styles.rowElemTwo}
              text={`${memoryString(disk.used)} / ${memoryString(
                disk.available,
              )}`}
            />
          </View>
        ))}
      </View>
    </InformationSchema>
  );
}

export function Actions({
  showRetry,
  refreshing,
  refresh,
  deleting,
  deleteAction,
  shutdownAction,
  shuttingDown,
}) {
  return (
    <View style={{...styles.container, ...styles.actionsContainer}}>
      <ActivityIndicator
        style={styles.actionsActivityIndicator}
        color="#000000"
        size="large"
        animating={refreshing}
      />
      <View style={styles.actionsButtonContainer}>
        <View
          style={{
            ...styles.actionsButton,
            display: showRetry ? 'flex' : 'none',
          }}>
          <Button title="Retry" loading={refreshing} onPress={refresh} />
        </View>
        <View
          style={{
            ...styles.actionsButton,
            display: !showRetry ? 'flex' : 'none',
          }}>
          <Button
            onPress={shutdownAction}
            loading={shuttingDown}
            title="Shutdown"
          />
        </View>
        <View style={styles.actionsButton}>
          <DeleteButton onPress={deleteAction} loading={deleting} />
        </View>
      </View>
    </View>
  );
}

function Info({text, style, textStyle}) {
  return (
    <View style={{...styles.info, ...style}}>
      <Text style={{...styles.infoText, ...textStyle}}>{text}</Text>
    </View>
  );
}

function InformationSchema({children, title}) {
  return (
    <View style={styles.container}>
      <View style={styles.subHeadingContainer}>
        <Text style={styles.subHeading}>{title}</Text>
      </View>

      <View style={styles.infoContainer}>{children}</View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    paddingVertical: 22,
    paddingHorizontal: 15,
  },
  veryBasicInfoContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  subHeadingContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  subHeading: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  infoContainer: {
    paddingTop: 10,
  },
  info: {
    paddingVertical: 1,
  },
  infoText: {
    fontSize: 15,
    color: `${COLOR.TEXT}CC`,
  },
  labels: {
    flexDirection: 'column',
    justifyContent: 'space-between',
    height: 45,
  },
  actionsContainer: {
    flexDirection: 'row',
  },

  actionsButtonContainer: {
    flex: 2,
    flexDirection: 'row',
  },
  actionsActivityIndicator: {
    flex: 1,
    alignItems: 'flex-start',
    marginRight: 5,
  },
  actionsButton: {
    flex: 1,
    marginHorizontal: 5,
  },
  row: {
    flex: 1,
    alignSelf: 'stretch',
    flexDirection: 'row',
  },
  rowElemOne: {
    flex: 1,
    alignSelf: 'stretch',
  },
  rowElemTwo: {
    flex: 2,
    alignSelf: 'stretch',
  },
  tableHeading: {
    fontWeight: 'bold',
  },
});
