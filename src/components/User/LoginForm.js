import React, {useState, useContext} from 'react';
import {View, StyleSheet} from 'react-native';
import fire from '../../requests/fire';
import UserContext from '../../context/UserContext';
import {InputBox} from '../Common/Inputs';
import {Button, Link} from '../Common/Buttons';
import FormContainer from '../Common/FormContainer';
import {Error} from '../Common/Texts';

function LoginForm({navigation}) {
  const setUser = useContext(UserContext)[1];
  const [loading, setLoading] = useState(false);
  const [form, setForm] = useState({
    username: '',
    password: '',
    error: '',
  });

  function handleChange(e) {
    const {name, value} = e.target;
    setForm({...form, [name]: value});
  }

  async function handleSubmit() {
    setLoading(true);
    try {
      await fire('login', form);
      const currentUser = await fire('getUser');
      setUser(currentUser);
    } catch (exception) {
      setForm({...form, error: exception});
      setLoading(false);
    }
  }

  return (
    <FormContainer image={require('../../../assets/images/login.png')}>
      <InputBox
        value={form.username}
        label="Username"
        placeholder="Your Username"
        name="username"
        onChange={handleChange}
      />
      <InputBox
        value={form.password}
        label="Password"
        placeholder="**********"
        name="password"
        secureTextEntry={true}
        onChange={handleChange}
      />
      <Error>{form.error}</Error>
      <View style={styles.bottomControl}>
        <Link
          onPress={() => navigation.navigate('ChangeServer')}
          title="Change Server"
        />
        <Button
          style={styles.button}
          onPress={handleSubmit}
          loading={loading}
          title="login"
        />
      </View>
    </FormContainer>
  );
}

const styles = StyleSheet.create({
  bottomControl: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  button: {
    width: 100,
  },
});

export default LoginForm;
