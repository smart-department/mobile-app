import React, {useState, useContext} from 'react';
import {View, StyleSheet, ActivityIndicator} from 'react-native';
import fire from '../../requests/fire';
import UserContext from '../../context/UserContext';
import UserOption from './UserOption';
import deleteUser from './deleteUser';

function UserOptionsContainer({navigation}) {
  const [user, setUser] = useContext(UserContext);
  const [loading, setLoading] = useState(false);

  async function logout() {
    if (loading) {
      return;
    }
    setLoading(true);
    try {
      await fire('logout');
    } catch (exception) {
      setLoading(false);
    } finally {
      setUser({});
    }
  }

  function deleteAccount() {
    if (loading) {
      return;
    }
    deleteUser(user.username, setLoading, setUser);
  }

  return (
    <View style={styles.container}>
      <View>
        {user.role === 'admin' && (
          <>
            <UserOption
              text="Add User"
              onPress={() => navigation.navigate('UserAdd')}
            />
            <UserOption
              text="All Users"
              onPress={() => navigation.navigate('UserAll')}
            />
          </>
        )}
        {user.username !== 'root' && (
          <UserOption text="Delete Account" onPress={deleteAccount} />
        )}
        <UserOption text="Logout" onPress={logout} />
      </View>
      {loading && <ActivityIndicator color="#000000" />}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    // marginTop: 20,
  },
  subHeading: {
    flexDirection: 'row',
    paddingVertical: 10,
    justifyContent: 'center',
  },
  subHeadingText: {
    fontSize: 20,
    paddingHorizontal: 15,
  },
});

export default UserOptionsContainer;
