import React, {useState, useContext} from 'react';
import {Text, View, StyleSheet} from 'react-native';
import {DeleteButton, Labels} from '../Common/Buttons';
import {dateString} from '../../helpers';
import deleteUser from './deleteUser';
import UserContext from '../../context/UserContext';
import {COLOR} from '../../helpers/constants';

function UserInfo({user, showDelete = false, refresh}) {
  const {username, role, created} = user;
  const [loading, setLoading] = useState(false);
  const [currentUser, setCurrentUser] = useContext(UserContext);

  return (
    <View style={styles.container}>
      <View>
        <View style={styles.mainInfoContainer}>
          <Text
            ellipsizeMode={'tail'}
            numberOfLines={1}
            style={styles.username}>
            {username}
          </Text>
          <Labels titles={[role]} danger={role === 'admin'} />
        </View>
        <Text style={styles.moreInfoText}>{dateString(created)}</Text>
      </View>
      {showDelete && username !== 'root' && (
        <View style={styles.buttonContainer}>
          <DeleteButton
            onPress={() => {
              deleteUser(
                username,
                setLoading,
                username === currentUser.username
                  ? () => setCurrentUser({})
                  : refresh,
              );
            }}
            loading={loading}
          />
        </View>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 15,
    paddingVertical: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderBottomWidth: 0.75,
    borderBottomColor: '#E2E8F0',
  },
  mainInfoContainer: {
    flex: 4,
    maxWidth: '80%',
    flexDirection: 'row',
    alignItems: 'center',
    paddingBottom: 7,
  },
  moreInfoText: {
    color: `${COLOR.TEXT}AA`,
  },
  username: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  buttonContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
});

export default UserInfo;
