import {ToastAndroid, Alert} from 'react-native';
import fire from '../../requests/fire';

function deleteUser(username, setLoading, callback = () => {}) {
  Alert.alert(
    'Confirm Deletion',
    `Are you sure you want to delete user '${username}'?`,
    [
      {
        text: 'Cancel',
        onPress: () => {},
        style: 'cancel',
      },
      {
        text: 'OK',
        onPress: async () => {
          setLoading(true);
          try {
            await fire('deleteUser', {}, {username});
            ToastAndroid.show(`Deleted '${username}'`, ToastAndroid.SHORT);
            callback();
          } catch (exception) {
            console.log(`Couldn't delete ${username}`);
            setLoading(false);
          }
        },
      },
    ],
    {cancelable: true},
  );
}

export default deleteUser;
