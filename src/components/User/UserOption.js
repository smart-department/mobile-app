import React from 'react';
import {Text, StyleSheet} from 'react-native';
import {Button} from '../Common/Buttons';
import {COLOR} from '../../helpers/constants';

function UserOption({text, onPress}) {
  return (
    <Button style={styles.container} onPress={onPress}>
      <Text style={styles.text}>{text}</Text>
    </Button>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'transparent',
    elevation: 0,
    borderRadius: 0,
    justifyContent: 'flex-start',
    paddingVertical: 20,
    paddingHorizontal: 15,
    borderBottomWidth: 0.75,
    borderBottomColor: '#E2E8F0',
  },
  text: {
    fontSize: 18,
    color: `${COLOR.TEXT}AA`,
  },
});

export default UserOption;
