import React, {useState} from 'react';
import {ToastAndroid} from 'react-native';
import {Button} from '../Common/Buttons';
import {InputBox, Option} from '../Common/Inputs';
import fire from '../../requests/fire';
import FormContainer from '../Common/FormContainer';
import {Error} from '../Common/Texts';

function UserAddForm({goBack}) {
  const [loading, setLoading] = useState(false);
  const [form, setForm] = useState({
    username: '',
    password: '',
    role: 'user',
    error: '',
  });

  function handleChange(e) {
    const {name, value} = e.target;
    setForm({...form, [name]: value});
  }

  async function handleSubmit() {
    setLoading(true);
    try {
      const currentUser = {...form};
      delete currentUser.error;
      await fire('createUser', currentUser);
      ToastAndroid.show(
        `Created '${currentUser.username}'`,
        ToastAndroid.SHORT,
      );
      goBack();
    } catch (exception) {
      ToastAndroid.show("Couldn't create user", ToastAndroid.SHORT);
      setForm({...form, error: exception});
      setLoading(false);
    }
  }

  return (
    <FormContainer>
      <InputBox
        value={form.username}
        label="Username"
        placeholder="Username for new user"
        name="username"
        onChange={handleChange}
      />
      <InputBox
        value={form.password}
        label="Password"
        placeholder="**********"
        name="password"
        secureTextEntry={true}
        onChange={handleChange}
      />
      <Option
        name="role"
        onChange={handleChange}
        label="Type of user"
        value={form.role}
        options={[
          {label: 'User', value: 'user'},
          {label: 'Admin', value: 'admin'},
        ]}
      />

      <Error>{form.error}</Error>
      <Button onPress={handleSubmit} loading={loading} title="Add User" />
    </FormContainer>
  );
}

export default UserAddForm;
