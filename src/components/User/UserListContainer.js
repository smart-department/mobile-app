import React, {useState, useEffect} from 'react';
import {FlatList} from 'react-native';
import fire from '../../requests/fire';
import UserInfo from './UserInfo';

function UserListContainer() {
  const [users, setUsers] = useState([]);
  const [refreshing, setRefreshing] = useState(false);
  useEffect(() => {
    if (users.length < 1) {
      fetchUsers();
    }
  });

  async function fetchUsers() {
    if (refreshing) {
      return;
    }
    setRefreshing(true);
    try {
      const allUsers = await fire('allUsers');
      setUsers(allUsers);
    } catch (exception) {
    } finally {
      setRefreshing(false);
    }
  }

  return (
    <FlatList
      data={users}
      renderItem={({item}) => (
        <UserInfo user={item} showDelete={true} refresh={fetchUsers} />
      )}
      keyExtractor={item => item.id}
      refreshing={refreshing}
      onRefresh={fetchUsers}
    />
  );
}

export default UserListContainer;
