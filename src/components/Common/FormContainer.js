import React from 'react';
import {View, Image, StyleSheet} from 'react-native';

function FormContainer(props) {
  return (
    <View {...props} style={{...styles.container, ...props.styles}}>
      {props.image && (
        <Image style={styles.image} source={props.image} resizeMode="contain" />
      )}
      {props.children}
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 30,
    paddingTop: 50,
    width: '100%',
  },
  image: {
    alignItems: 'center',
    paddingVertical: 15,
    marginTop: -15,
    maxHeight: 180,
    width: '100%',
    marginBottom: 20,
  },
});

export default FormContainer;
