import React from 'react';
import {
  View,
  Text,
  TouchableNativeFeedback,
  StyleSheet,
  ActivityIndicator,
  TouchableOpacity,
  Image,
} from 'react-native';
import {COLOR} from '../../helpers/constants';

export const Button = props => {
  const loading = props.loading || false;
  function handlePress() {
    if (!loading && typeof props.onPress === 'function') {
      props.onPress();
    }
  }
  return (
    <View style={props.styleParent}>
      <TouchableNativeFeedback {...props} onPress={handlePress}>
        <View style={{...styles.button, ...props.style}}>
          {props.loading ? (
            <ActivityIndicator
              color={COLOR.PRIMARY_TEXT}
              style={styles.activityIndicator}
            />
          ) : props.title ? (
            <Text style={styles.buttonText}>{props.title}</Text>
          ) : (
            props.children
          )}
        </View>
      </TouchableNativeFeedback>
    </View>
  );
};

export const DeleteButton = props => {
  return (
    <Button {...props} style={styles.deleteButton}>
      <Text style={{...styles.buttonText, ...styles.deleteButtonText}}>
        DELETE
      </Text>
    </Button>
  );
};

export const Link = props => {
  return (
    <TouchableOpacity activeOpacity={0.8} {...props}>
      {props.loading ? (
        <ActivityIndicator
          color={COLOR.PRIMARY_TEXT}
          style={styles.activityIndicator}
        />
      ) : props.title ? (
        <Text style={styles.linkText}>{props.title}</Text>
      ) : (
        props.children
      )}
    </TouchableOpacity>
  );
};

export const Labels = props => {
  return (
    <View style={{...styles.labels, ...props.style}}>
      {props.titles?.map(title => (
        <View
          key={title}
          style={{
            ...styles.label,
            ...(props.danger ? styles.labelDanger : {}),
          }}>
          <Text
            style={{
              ...styles.labelText,
              ...(props.danger ? styles.labelDangerText : {}),
            }}>
            {title?.toUpperCase()}
          </Text>
        </View>
      ))}
    </View>
  );
};

export const FloatingBottomButton = props => {
  return (
    <Button
      {...props}
      style={{
        ...styles.floatingButtonChild,
        ...styles.floatingButtonCommon,
        ...props.style,
      }}
      styleParent={{
        ...styles.floatingButtonParent,
        ...styles.floatingButtonCommon,
      }}>
      <Image
        source={require('../../../assets/images/plus.png')}
        style={styles.floatingButtonIcon}
      />
    </Button>
  );
};

const styles = StyleSheet.create({
  button: {
    flexDirection: 'row',
    justifyContent: 'center',
    paddingHorizontal: 10,
    paddingVertical: 10,
    backgroundColor: COLOR.PRIMARY,
    borderRadius: 5,
    elevation: 2,
  },

  deleteButton: {
    backgroundColor: COLOR.DANGER,
  },

  deleteButtonText: {
    color: COLOR.DANGER_TEXT,
  },

  buttonText: {
    textTransform: 'uppercase',
    fontWeight: 'bold',
    letterSpacing: 1,
    color: COLOR.PRIMARY_TEXT,
  },

  linkText: {
    textDecorationLine: 'underline',
    color: COLOR.PRIMARY_DARK,
    fontSize: 16,
  },

  labels: {
    flexDirection: 'row',
    alignItems: 'center',
  },

  label: {
    backgroundColor: COLOR.PRIMARY,
    justifyContent: 'center',
    paddingLeft: 4,
    paddingRight: 4,
    marginHorizontal: 5,
    paddingVertical: 2,
    borderRadius: 4,
  },

  labelDanger: {
    backgroundColor: COLOR.DANGER,
  },

  labelDangerText: {
    color: COLOR.DANGER_TEXT,
  },

  labelText: {
    fontWeight: 'bold',
    color: COLOR.PRIMARY_TEXT,
    letterSpacing: 1,
    fontSize: 12,
  },

  floatingButtonParent: {
    position: 'absolute',
    bottom: 25,
    right: 20,
    overflow: 'hidden',
    elevation: 5,
  },

  floatingButtonCommon: {
    height: 65,
    width: 65,
    borderRadius: 32.5,
  },

  floatingButtonChild: {
    justifyContent: 'center',
    backgroundColor: COLOR.PRIMARY,
    alignItems: 'center',
  },

  floatingButtonIcon: {
    height: 27,
    width: 27,
    tintColor: COLOR.PRIMARY_TEXT,
  },

  activityIndicator: {
    marginLeft: 2,
  },
});
