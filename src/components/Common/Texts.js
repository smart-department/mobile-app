import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {COLOR} from '../../helpers/constants';

export const Error = props => {
  return (
    <View style={styles.errorContainer}>
      <Text style={{...styles.text, ...styles.error}}>{props.children}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  text: {
    color: COLOR.TEXT,
  },
  errorContainer: {
    marginBottom: 15,
  },
  error: {
    color: COLOR.DANGER,
    fontStyle: 'italic',
  },
});
