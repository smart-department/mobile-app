import React from 'react';
import {View, Text, TextInput, StyleSheet} from 'react-native';
import {Picker} from '@react-native-community/picker';

export const InputBox = props => {
  const {name, onChange, label} = props;
  function handleChange(text) {
    onChange({target: {name, value: text}});
  }

  return (
    <View style={{...styles.textBoxContainer, ...props.style}}>
      <Text>{label}</Text>
      <TextInput
        {...props}
        style={styles.textBox}
        onChange={null}
        onChangeText={handleChange}
      />
    </View>
  );
};

export const Option = props => {
  const {name, onChange, label, value} = props;
  function handleChange(itemValue, itemIndex) {
    onChange({target: {name, value: itemValue}});
  }

  return (
    <View style={{...styles.textBoxContainer}}>
      <Text>{label}</Text>
      <Picker
        {...props}
        selectedValue={value}
        style={{...styles.optionContainer, ...styles.textBox}}
        onValueChange={handleChange}>
        {props.options?.map(option => (
          <Picker.Item
            key={option.value}
            label={option.label}
            value={option.value}
          />
        ))}
      </Picker>
    </View>
  );
};

const styles = StyleSheet.create({
  textBoxContainer: {
    alignSelf: 'stretch',
    flexDirection: 'column',
  },
  textBox: {
    alignSelf: 'stretch',
    backgroundColor: '#DDDDDD',
    borderRadius: 5,
    marginTop: 8,
    marginBottom: 15,
    fontSize: 15,
  },
  optionContainer: {
    alignSelf: 'stretch',
    height: 50,
  },
});
