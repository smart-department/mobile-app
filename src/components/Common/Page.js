import React from 'react';
import {View, StyleSheet} from 'react-native';
import {COLOR} from '../../helpers/constants';

function Page(props) {
  return <View style={styles.container}>{props.children}</View>;
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLOR.BACKGROUND,
  },
});

export default Page;
