export const dateString = function(dateInt) {
  const months = [
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'Jun',
    'Jul',
    'Aug',
    'Sep',
    'Oct',
    'Nov',
    'Dec',
  ];
  const date = new Date(parseInt(dateInt, 10) * 1000);
  if (!date) {
    return false;
  }

  let hour = date.getHours();
  hour = hour < 10 ? `0${hour}` : hour;
  let minute = date.getMinutes();
  minute = minute < 10 ? `0${minute}` : minute;

  return `${date.getDate()} ${
    months[date.getMonth()]
  } ${date.getFullYear()} ${hour}:${minute}`;
};

export const memoryString = function(num, base) {
  const symbols = ['B', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB'];
  const basePos =
    symbols.indexOf(base) === 0 ? -Infinity : symbols.indexOf(base);
  const exp =
    basePos !== -1 ? basePos - 1 : Math.floor(Math.log(num) / Math.log(1024));
  const loc = exp < -1 ? 0 : exp + 1;

  return `${
    !isNaN(num) && num === 0 ? '0' : (num / Math.pow(1024, exp)).toFixed(2)
  } ${symbols[loc]}`;
};
