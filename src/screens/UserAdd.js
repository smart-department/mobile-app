import React from 'react';
import Page from '../components/Common/Page';
import UserAddForm from '../components/User/UserAddForm';

function UserAdd({navigation}) {
  return (
    <Page>
      <UserAddForm goBack={() => navigation.goBack()} />
    </Page>
  );
}

export default UserAdd;
