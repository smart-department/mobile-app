import React from 'react';
import Page from '../components/Common/Page';
import SystemMoreInfo from '../components/System/SystemMoreInfo';

function SystemIndividiual({route, navigation}) {
  const {id} = route.params;
  return (
    <Page>
      <SystemMoreInfo navigation={navigation} id={id} />
    </Page>
  );
}

export default SystemIndividiual;
