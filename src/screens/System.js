import React, {useState, useEffect} from 'react';
import Page from '../components/Common/Page';
import SystemListContainer from '../components/System/SystemListContainer';

function System({navigation, route}) {
  const [refresh, setRefresh] = useState(0);

  useEffect(() => {
    const unsubscribe = navigation.addListener(
      'focus',
      () => {
        if (route?.params?.refresh) {
          route.params.refresh = false;
          setRefresh(c => c + 1);
        }
      },
      [navigation],
    );

    return unsubscribe;
  }, [navigation, route]);
  return (
    <Page>
      <SystemListContainer navigation={navigation} refresh={refresh} />
    </Page>
  );
}

export default System;
