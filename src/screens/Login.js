import React from 'react';
import LoginForm from '../components/User/LoginForm';
import Page from '../components/Common/Page';

function Login({navigation}) {
  return (
    <Page>
      <LoginForm navigation={navigation} />
    </Page>
  );
}

export default Login;
