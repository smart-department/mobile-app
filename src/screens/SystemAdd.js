import React from 'react';
import Page from '../components/Common/Page';
import SystemAddForm from '../components/System/SystemAddForm';

function SystemAdd({navigation}) {
  return (
    <Page>
      <SystemAddForm
        goBack={() => navigation.navigate('System', {refresh: true})}
      />
    </Page>
  );
}

export default SystemAdd;
