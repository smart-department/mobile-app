import React, {useState, useEffect} from 'react';
import {Text, StyleSheet, View} from 'react-native';
import {InputBox} from '../components/Common/Inputs';
import {Button, Link} from '../components/Common/Buttons';
import AsyncStorage from '@react-native-community/async-storage';
import Page from '../components/Common/Page';
import FormContainer from '../components/Common/FormContainer';

function ChangeServer({navigation}) {
  const [url, setUrl] = useState('');
  const [error, setError] = useState('');
  useEffect(() => {
    getBaseUrl();
  }, []);

  async function getBaseUrl() {
    try {
      const baseURL = await AsyncStorage.getItem('baseURL');
      setUrl(baseURL ? baseURL : '');
    } catch (e) {
      setUrl('');
    }
  }

  function handleChange(e) {
    setUrl(e.target.value);
  }

  async function handleSubmit() {
    console.log(`Changing app URL to '${url}'`);
    try {
      await AsyncStorage.setItem('baseURL', url);
      navigation.navigate('Login');
    } catch (e) {
      setError('Please try again');
    }
  }

  return (
    <Page>
      <FormContainer image={require('../../assets/images/change-server.png')}>
        <InputBox
          value={url}
          label="URL"
          placeholder="Enter URL here"
          name="url"
          onChange={handleChange}
        />
        <Text>{error}</Text>
        <View style={styles.bottomControl}>
          <Link onPress={() => navigation.navigate('Login')} title="Login" />
          <Button onPress={handleSubmit} title="save" />
        </View>
      </FormContainer>
    </Page>
  );
}

const styles = StyleSheet.create({
  bottomControl: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
});

export default ChangeServer;
