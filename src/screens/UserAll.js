import React from 'react';
import UserListContainer from '../components/User/UserListContainer';
import Page from '../components/Common/Page';

function UserAll() {
  return (
    <Page>
      <UserListContainer />
    </Page>
  );
}

export default UserAll;
