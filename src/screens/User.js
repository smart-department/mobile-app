import React, {useContext} from 'react';
import {ScrollView, StyleSheet} from 'react-native';
import UserContext from '../context/UserContext';
import UserInfo from '../components/User/UserInfo';
import UserOptionsContainer from '../components/User/UserOptionsContainer';
import Page from '../components/Common/Page';
function User({navigation}) {
  const user = useContext(UserContext)[0];

  return (
    <Page>
      <ScrollView style={StyleSheet.create({paddingTop: 15})}>
        <UserInfo user={user} />
        <UserOptionsContainer navigation={navigation} />
      </ScrollView>
    </Page>
  );
}

export default User;
