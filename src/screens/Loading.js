import React, {useEffect, useRef} from 'react';
import {View, Animated, Image, StyleSheet} from 'react-native';
import {COLOR} from '../helpers/constants';

function Loading() {
  const scaleAnim = useRef(new Animated.Value(1)).current;

  useEffect(() => {
    Animated.loop(
      Animated.timing(scaleAnim, {
        toValue: 1.5,
        useNativeDriver: true,
        duration: 400,
      }),
    ).start();
  });

  return (
    <View style={styles.conatiner}>
      <Animated.View
        style={{
          ...styles.iconContainer,
          ...styles.animatedContainer,
          transform: [{scale: scaleAnim}],
          opacity: scaleAnim.interpolate({
            inputRange: [1, 1.5],
            outputRange: [1, 0],
          }),
        }}
      />
      <View style={styles.iconContainer}>
        <Image
          style={styles.iconStyle}
          source={require('../../assets/images/icon.png')}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  conatiner: {
    flex: 1,
    backgroundColor: COLOR.PRIMARY,
    justifyContent: 'center',
    alignItems: 'center',
  },
  animatedContainer: {
    position: 'absolute',
    backgroundColor: '#F0F0F044',
  },
  iconStyle: {
    height: 140,
    width: 140,
  },
  iconContainer: {
    height: 220,
    width: 220,
    borderRadius: 110,
    backgroundColor: '#FFFFFF',
    justifyContent: 'center',
    alignItems: 'center',
  },
  headingText: {
    fontWeight: 'bold',
    fontSize: 25,
  },
});

export default Loading;
