import {endpoints} from './api';
import AsyncStorage from '@react-native-community/async-storage';
import {ToastAndroid} from 'react-native';

export default async function fire(type, body, getParams) {
  const controller = new AbortController();
  const signal = controller.signal;

  setTimeout(() => {
    controller.abort();
  }, 6000);

  const request = {...endpoints[type]};
  if (getParams) {
    const qs = Object.keys(getParams)
      .map(k => encodeURIComponent(k) + '=' + encodeURIComponent(getParams[k]))
      .join('&');
    request.url += qs ? `?${qs}` : '';
  }

  // const headers = new Headers({
  //   'Content-Type': 'application/json',
  // });
  let requestBody = {
    method: request.method,
    signal,
    // headers,
  };

  if (request.method.toUpperCase() !== 'GET') {
    requestBody.body = JSON.stringify(body);
  }

  let res, result, baseURL;
  try {
    baseURL = await AsyncStorage.getItem('baseURL');
    res = await fetch(`${baseURL}${request.url}`, requestBody);
  } catch (exception) {
    ToastAndroid.show('Server unreachable', ToastAndroid.SHORT);
    console.log(`Couldn't connect to '${baseURL}'`);
    throw exception.message;
  }

  try {
    result = await res.json();
    if (res.status < 200 || res.status > 299) {
      throw result;
    }
  } catch (exception) {
    console.log(exception);
    if (exception.statusCode) {
      throw exception.message;
    } else {
      ToastAndroid.show('Server response invalid', ToastAndroid.SHORT);
      throw 'Server response invalid';
    }
  }

  return result.data;
}
