export const baseURL = 'http://192.168.43.86/gokul/smart-department-server';

export const endpoints = {
  login: {
    url: '/user/login.php',
    method: 'POST',
  },

  logout: {
    url: '/user/login.php',
    method: 'DELETE',
  },

  createUser: {
    url: '/user.php',
    method: 'POST',
  },

  getUser: {
    url: '/user.php',
    method: 'GET',
  },

  deleteUser: {
    url: '/user.php',
    method: 'DELETE',
  },

  allUsers: {
    url: '/user/all.php',
    method: 'GET',
  },

  allSystems: {
    url: '/system/all.php',
    method: 'GET',
  },

  createSystem: {
    url: '/system.php',
    method: 'POST',
  },

  getSystem: {
    url: '/system.php',
    method: 'GET',
  },

  deleteSystem: {
    url: '/system.php',
    method: 'DELETE',
  },

  shutdownSystem: {
    url: '/system/shutdown.php',
    method: 'GET',
  },
};
