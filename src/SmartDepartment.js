import 'react-native-gesture-handler';
import React, {useEffect, useState, useContext, useCallback} from 'react';
import {StatusBar} from 'react-native';
import fire from './requests/fire';
import UserContext from './context/UserContext';
import {NavigationContainer} from '@react-navigation/native';
import AppNavigator from './navigator/AppNavigator';
import AdminNavigator, {FrontScreen} from './navigator/AdminNavigator';
import Loading from './screens/Loading';
import {COLOR} from './helpers/constants';

function SmartDepartment() {
  const [loading, setLoading] = useState(true);
  const [user, setUser] = useContext(UserContext);
  useEffect(() => {
    setTimeout(() => fetchCurrentUser(), 2000);
  }, [fetchCurrentUser]);

  const fetchCurrentUser = useCallback(async () => {
    try {
      const currentUser = await fire('getUser');
      setUser(currentUser);
      console.log(`Logged in as ${currentUser.username}`);
    } catch (exception) {
      console.log('Not logged in');
    } finally {
      setLoading(false);
    }
  }, [setUser]);

  function chooseNavigator() {
    if (user?.role === 'admin') {
      return <AdminNavigator />;
    } else if (user?.role === 'user') {
      return <FrontScreen />;
    } else {
      return <AppNavigator />;
    }
  }

  return (
    <NavigationContainer>
      <StatusBar
        barStyle="light-content"
        backgroundColor={COLOR.PRIMARY_DARK}
      />
      {loading ? <Loading /> : chooseNavigator()}
    </NavigationContainer>
  );
}

export default SmartDepartment;
